const DanItStudent = {
    checkPass: function () {
    const passDate = new Date(this._datePass);
    const currentDate = new Date();
    return passDate > currentDate;
    },
    changePass: function (date) {
        this._datePass = date;
    }

};

const student1 = {
    firstName: 'Piter',
    lastName: 'Snow',
    _datePass: '2019.02.18'
};
student1.__proto__ = DanItStudent;
const student2 = {
    firstName: 'Andrii',
    lastName: 'Rain',
    _datePass: '2019.08.18'
};

student2.__proto__ = DanItStudent;


// console.log(student1.checkPass());
console.log(student2.checkPass());
student1.changePass('2020-03-05');
console.log(student1.checkPass());
