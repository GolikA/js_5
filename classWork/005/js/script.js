const student1 = {
    'first Name': 'Алина',
    'last Name': 'Буреражденная',
    'tabel': {
        math: 10,
        biology: 11,
        geography: 3
    },
        getAverageScore: function () {
        let sumOfMarks = 0;
        let numberOfSubject = 0;
            for (let key in this.tabel){
                sumOfMarks += this.tabel[key];
                ++numberOfSubject;
            }
            return sumOfMarks/numberOfSubject;
        }
};
const student2 = {
    'first Name': 'Сергей',
    'last Name': 'Ланистер',
    'tabel': {
        math: 12,
        biology: 7,
        geography: 9
    },
    getAverageScore: function () {
        let sumOfMarks = 0;
        let numberOfSubject = 0;
        for (let key in this.tabel){
            sumOfMarks += this.tabel[key];
            ++numberOfSubject;
        }
        return sumOfMarks/numberOfSubject;
    }
};
const student3 = {
    'first Name': 'Мария',
    'last Name': 'Плющ',
    'tabel': {
        math: 4,
        biology: 9,
        geography: 6
    },
    getAverageScore: function () {
        let sumOfMarks = 0;
        let numberOfSubject = 0;
        for (let key in this.tabel){
            sumOfMarks += this.tabel[key];
            ++numberOfSubject;
        }
        return sumOfMarks/numberOfSubject;
    }
};

console.log(student1.getAverageScore());
console.log(student2.getAverageScore());
console.log(student3.getAverageScore());
