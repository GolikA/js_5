let {name, isNew = true, role = "user"} = {name: "Den", role: "registered", isNew: false};

function formUser({name, status = "new", role = "moderator"}) {
    return {name, role, status};
}

let registeredUser = formUser({name, isNew: "new", role, status: isNew});
console.log(registeredUser);