const circle = require('./circle');
console.log(circle.square(5));



function Square(b) {
    this.a = b;
    this.getSquare = function () {
        return this.a**2;
    }
    this.getDiag = function () {
      return this.a*(2**0.5);
    };
}

let square = new Square(4);
console.log(`square.a${square.a}
square.getSquare ${square.getSquare()}
square.getDiag ${square.getDiag()}`)


