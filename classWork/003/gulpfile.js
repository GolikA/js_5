const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require('browser-sync').create();

function style() {
    return (
        gulp
            .src("./src/scss/*.scss")
            .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
            .on("error", sass.logError)
            .pipe(gulp.dest("dist/css"))
    );
}

function watch(){
    // gulp.watch('./scss/*.scss', style)


}
/*exports.style = style;
exports.watch = watch;*/
function copyHTML(){
    return(
        gulp
            .src("./src/index.html")
            .pipe(gulp.dest("./dist"))
    )
}
function serve(){
    browserSync.init({
        server:{
            baseDir:'./dist'
        }
    });
}

gulp.watch('./src/*.html', copyHTML);
gulp.watch('./src/scss/**/*.scss', style);
// gulp.task('style', style);
gulp.task('copy-html', copyHTML);
gulp.task('serve', serve);
